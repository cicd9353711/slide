---
marp: false
footer: 'Warut C. 2023'
theme: gaia
_class: lead
# paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
---
# **RTSX**

Introduction to CI/CD with GitLab

---

## Content Overview

- Introduction to CI/CD
- GitLab Flow with Environment Branches
- GitLab CI
- Demo
  - Setup new project
  - Create Docker
- Setting up a GitLab Runner
- Conclusion
- Reference
- Project repository

---

## GitLab Flow with Environment Branches

![bg contain center:40% 75%](./assets/image-4.png)

---

![bg contain center:40% 100%](./assets/image-1.png)

---

## GitLab CI

![bg contain center:50% 70%](./assets/image-3.jpg)

---

<style scoped>
{
  font-size: 20px;
}
</style>
# Demo

## Setup new project

  1. At Gitlab Create new project
  2. New branch
      a. production
      b. staging
  3. Goto Setting
      - Repository → Protected branches
        - Branch
          - production
          - staging
        - Allow to merge → Maintainers
        - Allow to push → No one
      - Merge requests
        - Merge method → Fast-forward merge
  4. Project information → Members
      - Select a role → Developer
![bg contain right:50% 100%](./assets/image-2.png)

---

## Build and push docker

### Login to Gitlab registry

``` bash
$ docker login registry.gitlab.com
Username: <Enter your username>
Password: <Enter your password>
```

### Build image

``` bash
$ cd <to project folder>
$ docker compose build
$ docker compose push
```

---

## Build helm

```bash
cd helm/demo
helm dependency update 
helm install --namespace {{namespace}} --dependency-update --create-namespace {{releases name}} ./
```

Eg.

```bash
helm install --namespace demo --dependency-update --create-namespace demo-app ./
```

Uninstall

```bash
helm uninstall --namespace demo  demo-app
```

---

## Reference

 - https://www.youtube.com/watch?v=u9dgE2ghKQQ
 - https://docs.google.com/presentation/d/1K3hCFCCw6117rX4_3ccsh-xCM6v3zk7V8tyZc94qvdw/edit#slide=id.g1f25ffbe2c8_0_0
 - https://blog.searce.com/gitlab-ci-cd-to-deploy-applications-on-gke-using-shared-runner-47f8c42817ac

---

## Project Repository

  - https://gitlab.com/cicd9353711
